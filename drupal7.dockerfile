# If we weree to pull from private repo
# ARG REPO_LOCATION=nexus.usitc.gov/
# ARG BASE_VERSION=php8.2-apache-bookworm
# FROM ${REPO_LOCATION}drupal:${BASE_VERSION}
#FROM drupal:7.98-php8.2-apache-bookworm
FROM drupal:drupal:7-php7.4-fpm-alpine
# GET the Drupal Version from --build-arg and set the runtime environment variable
# if not set it will default to the DRUPAL_VERSION in the base image
ARG DRUPAL_VERSION 
ENV DRUPAL_VERSION=$DRUPAL_VERSION

#Configure Apache:
RUN { \
sed -i 's/Listen 80/Listen 8080/' /etc/apache2/ports.conf ; \
sed -i 's/:80/:8080/' /etc/apache2/sites-enabled/000-default.conf; \
cp /var/www/html/sites/default/default.settings.php /var/www/html/sites/default/settings.php ; \
chmod 777 /var/www/html/sites/default/settings.php ; \
}

# install the PHP extensions we need
RUN set -eux; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libfreetype6-dev \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
		libwebp-dev \
		libzip-dev \
    	git \
    	vim \
    	wget \
    	zip \
	; \
	\
	docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg=/usr \
		--with-webp \
	; \
	\
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
    \
# Install Drupal 
# composer require drupal/core-recommended:${DRUPAL_VERSION} drupal/core-composer-scaffold:${DRUPAL_VERSION} drupal/core-project-message:${DRUPAL_VERSION} --update-with-all-dependencies ;\
\
chmod 755 /var/www/ ;\
chmod -R 774 /var/www/html ;\
chown -R 774 /var/log/apache2 ;

